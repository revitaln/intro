<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/hello', function (){
    return 'Hello laravel';

});
Route::get('/student/{id}', function ($id = 'no student found'){
    return 'Hello student'.$id;

});

Route::get('/car/{id?}', function ($id = null){
    if(isset($id)){
        // TODO : validate for integer
        return "we got car $id";
    }else{
        return 'we need the id to find your car';
    }
    return 'Hello student'.$id;

});





Route::get('/comment/{id}', function ($id) {
    return view('comment' , compact('id'));
});

// וצרים ראוט שמציג את קובץ ה view

//Route::get('/comment', function () {
  //  return view('comment');
//});

// ex 5
Route::get('/users/{name?}/{email}', function ($name = null ,$email) {
    return view('users' ,compact('name','email'));
});
